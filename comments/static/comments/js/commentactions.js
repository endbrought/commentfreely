var cf_auth = document.getElementsByName('cf_auth')[0].value;
var csrfmiddlewaretoken = document.getElementsByName('csrfmiddlewaretoken')[0].value;
var cf_url = document.getElementsByName('cf_url')[0].value;

		$('#comments-container').comments({
			enableAttachments: true,
			readOnly: false,
			roundProfilePictures: false,
			currentUserId: document.getElementsByName('current_user_id')[0].value,
			profilePictureURL: document.getElementsByName('icon_url')[0].value,
			postCommentOnEnter: true,
			enableDeletingCommentWithReplies: true,
			currentUserIsAdmin: false,
			getComments: function(success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/get_comments/', 
					data: { csrfmiddlewaretoken : csrfmiddlewaretoken, cf_auth: cf_auth }, 
					success: function(userArray) { success(userArray); }, 
					error: error 
				});
			},
			getUsers: function(success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/get_users/', 
					data: { csrfmiddlewaretoken : csrfmiddlewaretoken, cf_auth: cf_auth }, 
					success: function(userArray) { success(userArray) }, error: error });
			},
			postComment: function(data, success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/create_comment/',
					data: { csrfmiddlewaretoken : csrfmiddlewaretoken, commentData: data, cf_auth: cf_auth },
					success: function(userArray) { success(userArray); },
					error: error
				});
			},
			putComment: function(data, success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/update_comment/',
					data: { csrfmiddlewaretoken : csrfmiddlewaretoken, commentData: data, cf_auth: cf_auth },
					error: error
				});
				success(data);
			},
			deleteComment: function(data, success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/delete_comment/',
					data: { csrfmiddlewaretoken : csrfmiddlewaretoken, commentData: data, cf_auth: cf_auth },
					error: error
				});
				success();
			},
			upvoteComment: function(data, success, error) {
				$.ajax({ 
					type: 'post', 
					url: cf_url+'/api/upvote_comment/',
					data: { csrfmiddlewaretoken : csrfmiddlewaretoken, commentData: data, cf_auth: cf_auth },
					error: error
				});
				success(data);
			},
			uploadAttachments: function(dataArray, success, error) {
				success(dataArray);
			},
			timeFormatter: function(time) {
    			return moment(time).fromNow();
  			},
		});

$('.textarea').bind('DOMSubtreeModified', function(){
	if ($(this).text().length > 10000) {
		$(this).text($(this).text().substr(0, 10000));
	}
});